@startuml

(Browse Product) as browse
(Search Product) as search
(Evaluate Product) as evaluate

Guest -up-> browse
Guest -down-> search
User -left-> browse
User -left-> search
User -down-> (Purchase)
User -> evaluate

@enduml