package net.flowas.modulecaert.testsupport;

import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

public class ClientFactory {
	private static ClientFactory instance=new ClientFactory();
	private Map<Class,Object> map=new HashMap<Class,Object>();
	private RemoteInvokeHandler handler=new RemoteInvokeHandler();
	private ClientFactory() {
		
		
	}
	public static ClientFactory getInstance(){
		return instance;
	}
	public <T> T getInterface(Class<T> clazz){
		T interfaceProxy = (T) map.get(clazz);
		if(interfaceProxy==null){
			Class[] interfaces={clazz};
			interfaceProxy=(T) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), interfaces, handler);
			map.put(clazz, interfaceProxy);
		}
		return interfaceProxy;
	}
}
